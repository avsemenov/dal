﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.IO;
using System.Linq;
using DAL.Data;
using DAL.Data.EFContext;
using DAL.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UTest.Data
{
    [TestClass]
    public class TestDB
    {
        private TestData _newRow0;

        [TestCleanup]
        public void TestInit()
        {
            Console.WriteLine("Init.");
        }

        [TestMethod]
        public void TestOpen()
        {
            Console.WriteLine("Test 1.");
            var ds = new DataStore();
            using (var dc = ds.GetContext())
            {
                Assert.IsNotNull(dc);
                Assert.AreEqual(0, dc.Records.Count());
            }
        }

        [TestMethod]
        public void TestSave()
        {
            var ds = new DataStore();
            using (var dc = ds.GetContext())
            {
                var rnd = new Random();
                var testVal = rnd.Next().ToString();
                Assert.IsNotNull(dc);

                var x = new Record
                {
                    Val = testVal,
                    RecordType = new RecordType {Name = "NetElement", Description = "Новый нет элемент"}
                };
                dc.Records.Add(x);
                Assert.AreEqual(0, x.Id);

                dc.SaveChanges();
                Assert.AreEqual(1, x.Id);

                x.Val = "eee";
                var z = dc.Records.Find(x.Id);
                Assert.AreEqual("eee", z.Val);

                dc.Entry(z).Reload();

                Assert.AreEqual(testVal, z.Val);
            }
        }

        [TestMethod]
        public void TestSchema()
        {
            var ds = new DataStore();
            using (var dc = ds.GetContext())
            {
                var ctx = dc.Set<TestData>();
                var newRow = ctx.Create();
                newRow.Title = "ura!";
                ctx.Add(newRow);
                dc.SaveChanges();

                var z = ctx.Count();
                Console.WriteLine(ctx.Count());

//                foreach (var complexRecord in z)
//                {
//                    Console.WriteLine(complexRecord.Id);
//                    complexRecord.Val = "foreach";
//                }
//                dc.SaveChanges();

                //dc.SaveChanges();
            }
        }

        [TestMethod]
        public void TestRepo()
        {
            _newRow0 = new TestData { Title = "ura! 0" };

            var ds = new DataStore();
            ds.AddHandler<TestData>(EntityState.Modified, testHandler);
            using (var dc = ds.GetContext())
            {
                var ctx = dc.Set<TestData>();
                ctx.Add(_newRow0);
                dc.SaveChanges();
                Console.WriteLine(_newRow0.GetHashCode());

                var newRow = ctx.Find(1);
                Console.WriteLine(newRow.GetHashCode());
                newRow.Title = "ura!";
                dc.SaveChanges();

                var z = ctx.Count();
                Console.WriteLine(ctx.Count());
                Console.WriteLine(_newRow0.Title);
            }

            using (var dc = ds.GetContext())
            {
                var ctx = dc.Set<TestData>();
                var newRow = ctx.Find(1);
                newRow.Title = "ura! 1";
                Console.WriteLine(newRow.GetHashCode());
                dc.SaveChanges();

                var test = dc.Entry(_newRow0);
                Console.WriteLine(test.GetHashCode());

                var z = ctx.Count();
                Console.WriteLine(ctx.Count());
                Console.WriteLine(_newRow0.Title);
            }

            ds.Save("testdbR");
        }

        private static void testHandler(object sender, EventArgs e)
        {
            Console.WriteLine(sender.GetType());
            Console.WriteLine(sender.GetHashCode());
            var s = (DbEntityEntry) sender;
            var ent = (TestData) s.Entity;
            Console.WriteLine(ent.GetHashCode());
        }
    }
}