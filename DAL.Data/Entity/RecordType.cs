﻿using System.ComponentModel.DataAnnotations.Schema;
using DAL.Data.Entity;

namespace DAL.Data.Entity
{
    [Table("RecordType")]
    public class RecordType : RecordBase
    {
        public string Name { get; set; }
        //public Type Type { get; set; }
        public string Description { get; set; }
    }
}

namespace DAL.Data.EntityEx
{
    [Table("exRecordType")]
    public class RecordType2 : RecordBase
    {
        public string Name { get; set; }
        //public Type Type { get; set; }
        public string Description { get; set; }
    }
}