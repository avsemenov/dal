﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Data.Entity
{
    public class Record : RecordBase
    {
        [Required]
        public RecordType RecordType { get; set; }

        public List<Record> Parents { get; set; }
        public List<Record> Childs { get; set; }
        public StringValue StringVal { get; set; }
        public IntValue IntVal { get; set; }
        public string Val { get; set; }
    }

    public class StringValue : RecordValue<string>
    {
    }

    public class IntValue : RecordValue<int>
    {
    }

    public class RecordValue<T> : RecordBase
    {
        public T Val { get; set; }
    }
}