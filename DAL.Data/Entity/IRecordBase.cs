﻿namespace DAL.Data.Entity
{
    public interface IRecordBase
    {
        int Id { get; set; }
    }
}