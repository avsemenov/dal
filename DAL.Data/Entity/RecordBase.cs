﻿namespace DAL.Data.Entity
{
    public class RecordBase : IRecordBase
    {
        public int Id { get; set; }
    }
}