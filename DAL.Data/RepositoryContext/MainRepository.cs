﻿using System;
using DAL.Data.EFContext;
using DAL.Data.Entity;

namespace DAL.Data.RepositoryContext
{

    public interface IRepositoryContext
    {
        // Save pending changes to the data store.
        void Commit();

        // Repositories
        IRepository<TestData> TestDatas { get; }
        IRepository<RecordType> RecordTypes { get; }
        IRepository<Record> ComplexRecords { get; }
    }

    class RepositoryContext
    {
        public void Commit()
        {
            throw new NotImplementedException();
        }

//        Repository<TestData> TestDatas { get; }
//        Repository<RecordType> RecordTypes { get; }
//        Repository<Record> ComplexRecords { get; }
    }
}
