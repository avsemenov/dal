using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using DAL.Data.EFContext;
using DAL.Data.Entity;

namespace DAL.Data
{
    public class DataStore
    {
        private readonly string _baseName = Path.GetTempFileName();

        public DataStore()
        {
            Handlers = new Dictionary<Type, Dictionary<EntityState, EventContainer>>();
        }

        public Dictionary<Type, Dictionary<EntityState, EventContainer>> Handlers { get; private set; }

        public void New()
        {
            File.Delete(_baseName);
        }

        public void Open(string fileName)
        {
            CopyBase(fileName + ".sdf", _baseName);
        }

        public void Save(string fileName)
        {
            CopyBase(_baseName, fileName + ".sdf");
        }

        private static void CopyBase(string from, string to)
        {
            if (File.Exists(from)) File.Copy(from, to, true);
        }

        public DalDbContext GetContext()
        {
            return new DalDbContext(_baseName, this);
        }

        public void OnSaveChanges(DbContext dbc)
        {
            foreach (var entry in dbc.ChangeTracker.Entries())
            {
                Dictionary<EntityState, EventContainer> stateHandlers;
                if (!Handlers.TryGetValue(entry.Entity.GetType(), out stateHandlers)) continue;
                EventContainer handler;
                if (stateHandlers.TryGetValue(entry.State, out handler)) handler.OnEvent(entry);
            }
        }

        public void AddHandler<T>(EntityState state, EventHandler handler) where T : IRecordBase
        {
            lock (Handlers)
            {
                Dictionary<EntityState, EventContainer> stateHandlers;
                if (!Handlers.TryGetValue(typeof (T), out stateHandlers))
                {
                    stateHandlers = new Dictionary<EntityState, EventContainer>();
                    Handlers.Add(typeof (T), stateHandlers);
                }
                if (!stateHandlers.ContainsKey(state)) stateHandlers.Add(state, new EventContainer());
                stateHandlers[state].Event += handler;
            }
        }
    }

    public class EventContainer
    {
        public event EventHandler Event;

        public void OnEvent(object sender)
        {
            var handler = Event;
            if (handler != null) handler(sender, EventArgs.Empty);
        }
    }
}