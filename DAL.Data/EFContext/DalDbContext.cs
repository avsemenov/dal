﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using DAL.Data.Entity;
using DAL.Data.EntityEx;

namespace DAL.Data.EFContext
{
    public class DalDbContext : DbContext
    {
        private readonly DataStore _ds;

        protected internal DalDbContext(string fileName, DataStore ds) : base("Data Source=" + fileName)
        {
            _ds = ds;
        }

        public DbSet<TestData> TestDatas { get; set; }
        public DbSet<Record> Records { get; set; }
        public DbSet<RecordType> RecordTypes { get; set; }
        public DbSet<RecordType2> RecordTypesEx { get; set; }

        public override int SaveChanges()
        {
            _ds.OnSaveChanges(this);
            return base.SaveChanges();
        }
    }
}