using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServerCompact;

namespace DAL.Data.EFContext
{
    public class Configuration : DbConfiguration
    {
        public Configuration()
        {
            SetDefaultConnectionFactory(new SqlCeConnectionFactory("System.Data.SqlServerCe.4.0"));
            SetProviderServices("System.Data.SqlServerCe.4.0", SqlCeProviderServices.Instance);
        }
    }
}